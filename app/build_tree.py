import os
from datetime import timedelta

def test():
    structure = []
    level = 1
    id = 1
    parent = '0-root'
    with open('app/output.xml') as f:
        lines = f.readlines()[1:]
    for i in range(len(lines)):
        lines[i] = lines[i].rstrip().lstrip()
        if not lines[i].startswith('</'):
            structure.append({'name':'-'.join([str(id), lines[i].split()[0][1:-1]]), 'level': level, 'start': float(lines[i].split()[1]),
                                    'stop': 0, 'time': 0, 'parent':parent,
                                     'id':id, 'count': 1})
            parent = '-'.join([str(id), lines[i].split()[0][1:-1]])
            level += 1
            id += 1
        else:
            for j in range(len(structure)):
                if structure[j]['name'].split('-')[1] == lines[i].split()[0][2:-1] and structure[j]['stop'] == 0:
                    structure[j]['stop'] = float(lines[i].split()[1])
                    level -= 1
                    for j in range(len(structure)-1, -1, -1):
                        if structure[j]['level'] == level-1:
                            parent = structure[j]['name']
                            break
                    break
            try:
                if lines[i+1].startswith('</'):
                    for j in range(len(structure)-1, -1, -1):
                        if structure[j]['level'] == level - 1:
                            parent = structure[j]['name']
                            break
            except:pass


    for i in range(len(structure)):
        if structure[i]['time'] <= 0:
            for j in range(len(structure), -1, -1):
                if structure[j-1]['stop'] > 0:
                    structure[i]['stop'] = structure[j-1]['stop']
                    break
        structure[i]['time'] = round(structure[i]['stop'] - structure[i]['start'], 2)
    answer = {}
    for i in range(len(structure)):
        # input(structure[i])
        if len(answer) == 0:
            # input('0 added')
            answer[structure[i]['name']] = {'time': structure[i]['time'],
                                            'parent': structure[i]['parent'],
                                            'count': 1,
                                            'level': structure[i]['level'],
                                            'name': structure[i]['name']}
        else:
            pare = False
            for j in list(answer.keys()):
                # print(structure[i]['name'].split('-')[1])
                # print(answer[j]['name'].split('-')[1])
                if structure[i]['name'].split('-')[1] == answer[j]['name'].split('-')[1]:
                    if structure[i]['level'] == answer[j]['level'] and structure[i]['parent'] == answer[j]['parent']:
                        # input('пара')
                        # print(answer[j]['time'])
                        # print(structure[i]['time'])
                        answer[j]['time'] = round((answer[j]['time'] * answer[j]['count'] + structure[i]['time']) /
                                                  (answer[j]['count'] + 1), 2)
                        answer[j]['count'] += 1
                        # input(answer[j])
                        pare=True
                        break

                    else:
                        continue
            if not pare:
                # input('нет пары')
                answer[structure[i]['name']] = {'time': structure[i]['time'],
                                                'parent': structure[i]['parent'],
                                                'count': 1,
                                                'level': structure[i]['level'],
                                                'name': structure[i]['name']}

    for i in answer:
        answer[i]['parent'] = answer[i]['parent'].split('-')[1]
        answer[i]['name'] = answer[i]['name'].split('-')[1]
    return answer



