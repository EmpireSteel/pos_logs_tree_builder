# coding=utf-8
import argparse
from datetime import datetime
import os


def parser(file, output, structure):
    start = 0
    with open(output, 'w', encoding='utf') as f:
        f.write('<?xml version="1.0" encoding="UTF-8"?>\n')
        for line in file:
            if start == 0:
                if '[PROCESS_POS_FUNCTION]' in line:
                    date = datetime.strptime(line[:22], '%Y-%m-%d %H:%M:%S:%f').timestamp()
                    start = 1
                    tree = line.split(' [PROCESS_POS_FUNCTION] ')[1]
                    sections = tree.split(' -> ')
                    sections[-1] = sections[-1][:-1]
                    deep = len(sections)
                    for i in range(deep-1, -1, -1):
                        f.write('\t' * (deep - 1 - i) + '<' + sections[i] + '> ' + str(date) + '\n')
                    sections.reverse()
            else:
                if '[PROCESS_POS_FUNCTION]' in line:
                    date = datetime.strptime(line[:22], '%Y-%m-%d %H:%M:%S:%f').timestamp()
                    new_tree = line.split(' [PROCESS_POS_FUNCTION] ')[1]
                    new_section = new_tree.split(' -> ')
                    new_section[-1] = new_section[-1][:-1]
                    new_section.reverse()
                    new_deep = len(new_section)
                    if new_deep > deep:
                        for i in range(deep, new_deep):
                            if new_section[i] == ' ':
                                print(new_section)
                                print(line)
                            f.write('\t' * i + '<' + new_section[i] + '> ' + str(date) + '\n')
                        sections = new_section
                        deep = new_deep
                    elif new_deep < deep:
                        for i in range(new_deep, deep)[::-1]:
                            f.write('\t' * i + '</' + sections[i] + '> ' + str(date) + '\n')
                        deep = new_deep
                        sections = new_section
                else:
                    if structure == None:
                        l = list(line)
                        for i in range(len(l)):
                            if 32 <= ord(l[i]) <= 126 or 1040 <= ord(l[i]) <= 1103:
                                if l[i] == '<':
                                    l[i] = '&lt;'
                                if l[i] == '>':
                                    l[i] = '&gt;'
                                if l[i] == '"':
                                    l[i] = '&quot;'
                                if l[i] == "'":
                                    l[i] = '&apos;'
                                if l[i] == '&':
                                    l[i] = '&amp;'
                            else:
                                l[i] = ' '
                        line = ''.join(l)
                        if '&lt;' not in line:
                            f.write('\t' * deep + line)
                        else:
                            data = " ".join(line.split()[4:])
                            f.write('\t' * deep + data + '<br/>\n')
        for i in range(deep)[::-1]:
            f.write('\t' * i + '</' + sections[i] + '> 0\n')


def reader(file):
    with open(file, 'r', encoding='utf') as f:
        lines = f.readlines()
    return lines


# if __name__ == '__main__':
#     arg_parser = argparse.ArgumentParser(description='Парсинг std_pos.log в xml')
#     arg_parser.add_argument('-l', '--log', help='Файл std_pos.log')
#     arg_parser.add_argument('-s', '--structure', help='Только структура')
#     args = arg_parser.parse_args()
#     structure = args.structure
#     log = str(args.log)
#     log_lines = reader(log)
#     parser(log_lines, log, structure)



def parse():
    file = reader(os.path.join(os.getcwd(), 'app', 'logs', 'all_log.log'))
    parser(file, os.path.join(os.getcwd(), 'app', 'output.xml'), 1)