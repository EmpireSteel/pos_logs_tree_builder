from app import app, merge_logs, parser, build_tree
from flask import render_template




@app.route('/')
@app.route('/index')
def index():
    print(123)
    merge_logs.merge()
    parser.parse()
    answer = build_tree.test()
    for i in list(answer.keys()):
        print(answer[i])
    return render_template('main.html', answer=answer)