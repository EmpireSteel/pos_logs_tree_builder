import os


def merge():
    with open(os.path.join(os.getcwd(), 'app', 'logs', 'all_log.log'), 'w', encoding='utf') as log:
        for i in os.listdir(os.path.join(os.getcwd(), 'app', 'logs')):
            if i.startswith('std'):
                with open(os.path.join(os.getcwd(), 'app', 'logs', i), 'r', encoding='utf') as f:
                    for line in f.readlines():
                        log.write(line)


